package com.example.weblabwork2.services;

import com.example.weblabwork2.beans.Dot;
import com.example.weblabwork2.other.Shape;
import com.example.weblabwork2.beans.ProcessedDot;

import java.util.Map;
import java.util.Optional;

public class DotsService {
    private static final String X_KEY = "x-coordinate";
    private static final String Y_KEY = "y-coordinate";
    private static final String R_KEY = "r-value";

    private final Shape shape;

    public DotsService(Shape shape) {
        this.shape = shape;
    }

    public Optional<Dot> verifyAndCreate(Map<String, String[]> qstring) {
        if (!quarriesExist(qstring))
            return Optional.empty();
        try {
            Dot dot = new Dot(
                    Float.parseFloat(qstring.get(X_KEY)[0]),
                    Float.parseFloat(qstring.get(Y_KEY)[0]),
                    Float.parseFloat(qstring.get(R_KEY)[0])
            );
            if (
                    !inRange(-5, 3, dot.getX()) ||
                    !inRange(-3, 3, dot.getY()) ||
                    !inRange(1, 3, dot.getR())
            )  {
                return Optional.empty();
            }
            return Optional.of(dot);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    public ProcessedDot processDot(Dot dot) {
        long startProc = System.currentTimeMillis();
        boolean hit = shape.contain(dot);
        return new ProcessedDot(dot, System.currentTimeMillis()-startProc, hit);
    }

    public static boolean quarriesExist(Map<String, String[]> qstring) {
        return qstring.containsKey(X_KEY) && qstring.containsKey(Y_KEY)
                && qstring.containsKey(R_KEY);
    }

    private static boolean inRange(float start, float stop, float num) {
        return num >= start && num <= stop;
    }

}
