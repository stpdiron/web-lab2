package com.example.weblabwork2.controllers;

import com.example.weblabwork2.services.DotsService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "controllerServlet", value = "/")
public class ControllerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().log((String) req.getSession().getAttribute("value"));
        if (DotsService.quarriesExist(req.getParameterMap())) {
            getServletContext().getNamedDispatcher("areaCheckServlet").forward(req, resp);
        } else {
            resp.setContentType("text/html");
            getServletContext().getRequestDispatcher("/main-page.jsp").forward(req, resp);
        }
    }



}
