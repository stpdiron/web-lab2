package com.example.weblabwork2.controllers;

import com.example.weblabwork2.beans.DotsData;
import com.example.weblabwork2.other.ConcreteShape;
import com.example.weblabwork2.services.DotsService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "areaCheckServlet", value = "/check")
public class AreaCheckServlet extends HttpServlet {
    private DotsService dotsService;

    @Override
    public void init() throws ServletException {
        dotsService = new DotsService(new ConcreteShape());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        dotsService.verifyAndCreate(req.getParameterMap()).ifPresentOrElse(
                (dot) -> {
                    DotsData table = (DotsData) req.getSession().getAttribute("dotsData");
                    if (table == null) {
                        table = new DotsData();
                    }
                    table.setShowError(false);
                    table.getDots().addFirst(dotsService.processDot(dot));
                    req.getSession().setAttribute("dotsData", table);
                },
                () -> {
                    DotsData table = (DotsData) req.getSession().getAttribute("dotsData");
                    if (table == null) {
                        table = new DotsData();
                    }
                    table.setShowError(true);
                    req.getSession().setAttribute("dotsData", table);
                }
        );
        try {
            resp.setContentType("text/html");
            req.getRequestDispatcher("/components/table.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            resp.setStatus(500);
        }
    }
}
