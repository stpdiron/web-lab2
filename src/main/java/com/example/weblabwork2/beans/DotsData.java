package com.example.weblabwork2.beans;

import java.io.Serializable;
import java.util.LinkedList;

public class DotsData implements Serializable {
    LinkedList<ProcessedDot> dots = new LinkedList<>();
    boolean showError = false;

    public boolean isShowError() {
        return showError;
    }

    public void setShowError(boolean showError) {
        this.showError = showError;
    }

    public void setDots(LinkedList<ProcessedDot> dots) {
        this.dots = dots;
    }

    public LinkedList<ProcessedDot> getDots() {
        return dots;
    }
}
