package com.example.weblabwork2.beans;

import java.io.Serializable;

public class Dot implements Serializable {
    private float x;
    private float y;
    private float r;

    public Dot() {}
    public Dot(float x, float y, float r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setR(float r) {
        this.r = r;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getR() {
        return r;
    }
}
