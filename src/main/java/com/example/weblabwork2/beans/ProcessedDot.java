package com.example.weblabwork2.beans;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ProcessedDot extends Dot implements Serializable {
    private long workTime;
    private ZonedDateTime processedAt;
    private boolean hit;

    public ProcessedDot() {}

    public ProcessedDot(Dot dot, long workTime, boolean hit) {
        setR(dot.getR());
        setX(dot.getX());
        setY(dot.getY());
        setHit(hit);
        setWorkTime(workTime);
        ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
        setProcessedAt(now);
    }
    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public long getWorkTime() {
        return workTime;
    }

    public void setWorkTime(long workTime) {
        this.workTime = workTime;
    }

    public ZonedDateTime getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(ZonedDateTime processedAt) {
        this.processedAt = processedAt;
    }
}
