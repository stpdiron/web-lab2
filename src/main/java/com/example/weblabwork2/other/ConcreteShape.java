package com.example.weblabwork2.other;

import com.example.weblabwork2.beans.Dot;

public final class ConcreteShape extends Shape {
    @Override
    public boolean contain(Dot dot) {
        return ( upperL(dot) && Math.pow(dot.getX(), 2) + Math.pow(dot.getY(), 2) <= Math.pow(dot.getR()/2, 2) )
                || ( upperR(dot) && dot.getY() <= dot.getR() && dot.getX() <= dot.getR() )
                || ( lowerR(dot) && dot.getY() >= dot.getX() - dot.getR() / 2 );
    }
}
