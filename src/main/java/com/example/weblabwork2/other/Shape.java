package com.example.weblabwork2.other;

import com.example.weblabwork2.beans.Dot;

public abstract class Shape {
    public abstract boolean contain(Dot dot);

    protected boolean upperL(Dot dot) {
        return dot.getY() >= 0 && dot.getX() < 0;
    }

    protected boolean upperR(Dot dot) {
        return dot.getY() >= 0 && dot.getX() >= 0;
    }

    protected boolean lowerR(Dot dot) {
        return dot.getY() < 0 && dot.getX() >= 0;
    }

    protected boolean lowerL(Dot dot) {
        return dot.getY() < 0 && dot.getX() < 0;
    }
}
