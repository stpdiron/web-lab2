<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" type="text/css" rel="stylesheet">
    <%--    <link href="./css/styles.css" type="text/css" rel="stylesheet">--%>
        <title>Лабораторная работа 1</title>
        <style type="text/css">
            <%@include file="css/styles.css"%>
        </style>
    </head>
    <body class="dark-body">
        <%-- header--%>
        <jsp:include page="components/header.jsp"/>
        <%-- form --%>
        <jsp:include page="components/form.jsp"/>
        <%-- table --%>
        <div id="table-wrapper">
            <jsp:include page="components/table.jsp"/>
        </div>
    </body>
<%--    <script src="js/form_handler.js"></script>--%>
    <script><%@include file="/js/draw.js"%></script>
</html>