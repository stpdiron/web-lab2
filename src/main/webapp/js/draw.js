const canvas = document.getElementById("coordinates-canvas");
let frame = prepareDrawing(canvas);

const figuresColor = "#1890ff"
const cursorColor = "#92eba0"
const cellsColor = "#ebedf2"
const hitColor = "#de0d45"

canvas.onclick = async function (event) {
    const rect = event.target.getBoundingClientRect();
    const rRadio = document.querySelector('input[name="r-value"]:checked');
    const x = (event.clientX - rect.left - frame.centerX) / frame.halfRX / 2 * rRadio.value;
    const y = (frame.sizeY - (event.clientY - rect.top) - frame.centerY) / frame.halfRY / 2 * rRadio.value;
    console.log(rRadio.value);
    await makeNewDot(x, y, rRadio.value);
}

canvas.onmousemove = function (event) {
    const ctx = canvas.getContext('2d');
    redraw(ctx);
    const rect = event.target.getBoundingClientRect();
    const x = event.clientX - rect.left; //x position within the element.
    const y = event.clientY - rect.top;
    drawDot(ctx, x, y, cursorColor);
}

const onResize = function(event) {
    if (canvas.getContext){
        const ctx = canvas.getContext('2d')
        frame = prepareDrawing(canvas)
        canvas.width = frame.sizeX
        canvas.height = frame.sizeY
        redraw(ctx);
    }
}

document.body.onresize = onResize;
document.body.onload = onResize;

function redraw(ctx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    draw(ctx, frame);
    const params = getDotDetails();
    if (params) {
        drawDot(
            ctx,
            frame.centerX + params.x * (frame.halfRX * 2 / params.r),
            frame.centerY - params.y * (frame.halfRY * 2 / params.r),
            params.hit ? hitColor : cellsColor
        );
    }
}

function getDotDetails() {
    const x_cells = document.getElementsByClassName("x-cell");
    const r_cells = document.getElementsByClassName("r-cell");
    const y_cells = document.getElementsByClassName("y-cell");
    const hit_cells = document.getElementsByClassName("hit-cell");

    if (x_cells.length == 0 || r_cells == 0 || y_cells == 0 || hit_cells == 0)
        return null;

    const x = parseFloat(x_cells[0].innerHTML);
    const y = parseFloat(y_cells[0].innerHTML);
    const r = parseFloat(r_cells[0].innerHTML);
    const hit = hit_cells[0].innerHTML == " <b> + </b> ";

    if (x == null || y == null || r == null)
        return null
    return {x, y, r, hit}
}

function drawDot(ctx, x, y, color) {
    ctx.fillStyle = color
    ctx.beginPath()
    ctx.arc(x, y, 4, 0, Math.PI*2, false)
    ctx.fill()
    ctx.closePath()
}

function prepareDrawing(canvas) {
    const sizeX = window.innerWidth > 1000 ? window.innerWidth * 0.23 : window.innerWidth * 0.8
    const sizeY = sizeX

    const centerX = sizeX / 2
    const centerY = sizeY / 2
    const halfRX = (sizeX*0.8) / 4
    const halfRY = (sizeY*0.8) / 4

    return {sizeX, sizeY, centerX, centerY, halfRX, halfRY}
}

function draw(ctx, frame) {
    ctx.fillStyle = figuresColor
    ctx.strokeStyle = cellsColor
    ctx.lineWidth = 2
    ctx.font = "15px serif";

    // Четверть круга
    ctx.beginPath()
    ctx.moveTo(frame.centerX-frame.halfRX, frame.centerY)
    ctx.arc(frame.centerX, frame.centerY, frame.halfRX, Math.PI,  Math.PI*1.5, false)
    ctx.lineTo(frame.centerX, frame.centerY)
    ctx.lineTo(frame.centerX - frame.halfRX, frame.centerY)
    ctx.fill()
    ctx.closePath()

    // Прямоугольник
    ctx.beginPath()
    ctx.moveTo(frame.centerX, frame.centerY)
    ctx.lineTo(frame.centerX, frame.centerY - frame.halfRY*2)
    ctx.lineTo(frame.centerX + frame.halfRX*2, frame.centerY - frame.halfRY*2)
    ctx.lineTo(frame.centerX + frame.halfRX*2, frame.centerY)
    ctx.lineTo(frame.centerX, frame.centerY)
    ctx.fill()
    ctx.closePath()

    // Триугольник
    ctx.beginPath()
    ctx.moveTo(frame.centerX, frame.centerY)
    ctx.lineTo(frame.centerX, frame.centerY + frame.halfRY)
    ctx.lineTo(frame.centerX + frame.halfRX, frame.centerY)
    ctx.lineTo(frame.centerX, frame.centerY)
    ctx.fill()
    ctx.closePath()

    ctx.fillStyle = cellsColor
    // Оси
    ctx.beginPath()
    ctx.moveTo(frame.centerX, frame.sizeY)
    ctx.lineTo(frame.centerX, 0)
    ctx.moveTo(0, frame.centerY)
    ctx.lineTo(frame.sizeX, frame.centerY)
    ctx.closePath();
    ctx.stroke();

    // стрелка оси Y
    ctx.beginPath()
    ctx.lineTo(frame.centerX - 5, 8)
    ctx.lineTo(frame.centerX + 5, 8)
    ctx.lineTo(frame.centerX, 0)
    ctx.closePath()
    ctx.fill()

    // стрелка оси X
    ctx.beginPath()
    ctx.lineTo(frame.sizeX, frame.centerY)
    ctx.lineTo(frame.sizeX - 8, frame.centerY + 5)
    ctx.lineTo(frame.sizeX - 8, frame.centerY - 5)
    ctx.lineTo(frame.sizeX, frame.centerY)
    ctx.closePath()
    ctx.fill()

    // Разметка оси Y
    ctx.beginPath()
    // R/2
    ctx.moveTo(frame.centerX - 3, frame.centerY - frame.halfRY)
    ctx.lineTo(frame.centerX + 3, frame.centerY - frame.halfRY)
    ctx.fillText("R/2", frame.centerX + 8, frame.centerY - frame.halfRY);
    // R
    ctx.moveTo(frame.centerX - 3, frame.centerY - 2*frame.halfRY)
    ctx.lineTo(frame.centerX + 3, frame.centerY - 2*frame.halfRY)
    ctx.fillText("R", frame.centerX + 8, frame.centerY - 2*frame.halfRY);
    // -R/2
    ctx.moveTo(frame.centerX - 3, frame.centerY + frame.halfRY)
    ctx.lineTo(frame.centerX + 3, frame.centerY + frame.halfRY)
    ctx.fillText("-R/2", frame.centerX + 8, frame.centerY + frame.halfRY);
    // -R
    ctx.moveTo(frame.centerX - 3, frame.centerY + 2*frame.halfRY)
    ctx.lineTo(frame.centerX + 3, frame.centerY + 2*frame.halfRY)
    ctx.fillText("-R", frame.centerX + 8, frame.centerY + 2*frame.halfRY);

    // Разметка оси X
    // -R
    ctx.moveTo(frame.centerX - 2*frame.halfRX, frame.centerY - 3)
    ctx.lineTo(frame.centerX - 2*frame.halfRX, frame.centerY + 3)
    ctx.fillText("-R", frame.centerX - 2*frame.halfRX, frame.centerY - 8);
    // -R/2
    ctx.moveTo(frame.centerX - frame.halfRX, frame.centerY - 3)
    ctx.lineTo(frame.centerX - frame.halfRX, frame.centerY + 3)
    ctx.fillText("-R/2", frame.centerX - frame.halfRX, frame.centerY - 8);
    // R/2
    ctx.moveTo(frame.centerX + frame.halfRX, frame.centerY - 3)
    ctx.lineTo(frame.centerX + frame.halfRX, frame.centerY + 3)
    ctx.fillText("R/2", frame.centerX + frame.halfRX, frame.centerY - 8);
    // R
    ctx.moveTo(frame.centerX + 2*frame.halfRX, frame.centerY - 3)
    ctx.lineTo(frame.centerX + 2*frame.halfRX, frame.centerY + 3)
    ctx.fillText("R", frame.centerX + 2*frame.halfRX, frame.centerY - 8);
    ctx.closePath()
    ctx.stroke()
}