const SERVER_URL = window.location.href;

document.getElementById("send-btn").onclick = async function () {
    const xRadio = document.querySelector('input[name="x-coordinate"]:checked');
    const rRadio = document.querySelector('input[name="r-value"]:checked');
    const yField = document.getElementById("y-coordinate");
    if (!checkNull(xRadio) || !checkNull(yField) || !checkNull(rRadio) || !validate(yField.value))
        return;
    console.log(xRadio.value, parseFloat(yField.value).toString(), rRadio.value);
    await makeNewDot(xRadio.value, parseFloat(yField.value).toString(), rRadio.value);
}

async function makeNewDot(x, y, r) {
    const table = await getChangedTable(x, y, r);
    await insertTable(table);
    redraw(canvas.getContext('2d'));
}

function validate(yRaw) {
    let yNumb = parseFloat(yRaw);
    if (isNaN(yNumb) || yNumb < -3 || yNumb > 3) {
        alert("Y value must be greater than -3 and lower than 3");
        return false;
    }
    return true;
}

function checkNull(value) {
    if (!value)
        alert("Null exception!");
    return value != null;
}

async function getChangedTable(x, y, r) {
    const url = new URL(SERVER_URL);
    url.searchParams.set("x-coordinate", x);
    url.searchParams.set("y-coordinate", y);
    url.searchParams.set("r-value", r);
    console.log(url.toString());
    const resp =  await fetch(url.toString(), {
        method: 'GET'
    });
    if (!resp.ok) {
        alert("bad response: " + resp.statusText);
        throw new Error(resp.statusText);
    }
    return await resp.text()
}

async function insertTable(table) {
    const tableWrapper = document.getElementById("table-wrapper");
    tableWrapper.innerHTML = table;
}