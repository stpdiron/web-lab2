<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="content-block bordered inlined">
    <canvas id="coordinates-canvas" class="bordered"></canvas>
    <div class="main-form">
        <fieldset class="radios">
            <legend>Координата X:</legend>
            <div>
                <input type="radio" id="-5-radio" value="-5" name="x-coordinate" checked>
                <label for="-5-radio">-5</label>
            </div>
            <div>
                <input type="radio" id="-4-radio" value="-4" name="x-coordinate" >
                <label for="-4-radio">-4</label>
            </div>
            <div>
                <input type="radio" id="-3-radio" value="-3" name="x-coordinate" >
                <label for="-3-radio">-3</label>
            </div>
            <div>
                <input type="radio" id="-2-radio" value="-2" name="x-coordinate" >
                <label for="-2-radio">-2</label>
            </div>
            <div>
                <input type="radio" id="-1.5-radio" value="-1.5" name="x-coordinate">
                <label for="-1.5-radio">-1.5</label>
            </div>
            <div>
                <input type="radio" id="-1-radio" value="-1" name="x-coordinate">
                <label for="-1-radio">-1</label>
            </div>
            <div>
                <input type="radio" id="-0.5-radio" value="-0.5" name="x-coordinate">
                <label for="-0.5-radio">-0.5</label>
            </div>
            <div>
                <input type="radio" id="0-radio" value="0" name="x-coordinate">
                <label for="0-radio">0</label>
            </div>
            <div>
                <input type="radio" id="0.5-radio" value="0.5" name="x-coordinate">
                <label for="0.5-radio">0.5</label>
            </div>
            <div>
                <input type="radio" id="1-radio" value="1" name="x-coordinate">
                <label for="1-radio">1</label>
            </div>
            <div>
                <input type="radio" id="1.5-radio" value="1.5" name="x-coordinate">
                <label for="1.5-radio">1.5</label>
            </div>
            <div>
                <input type="radio" id="2-radio" value="2" name="x-coordinate">
                <label for="2-radio">2</label>
            </div>
            <div>
                <input type="radio" id="3-radio" value="3" name="x-coordinate">
                <label for="3-radio">3</label>
            </div>
        </fieldset>

        <fieldset>
            <legend>Координата Y:</legend>
            <input type="text" id="y-coordinate" name="y-coordinate" value="1">
        </fieldset>

        <fieldset class="radios">
            <legend>Значение R:</legend>
            <div>
                <input type="radio" id="r-1-radio" value="1" name="r-value" checked>
                <label for="r-1-radio">1</label>
            </div>
            <div>
                <input type="radio" id="r-1.5-radio" value="1.5" name="r-value">
                <label for="r-1.5-radio">1.5</label>
            </div>
            <div>
                <input type="radio" id="r-2-radio" value="2" name="r-value">
                <label for="r-2-radio">2</label>
            </div>
            <div>
                <input type="radio" id="r-2.5-radio" value="2.5" name="r-value">
                <label for="r-2.5-radio">2.5</label>
            </div>
            <div>
                <input type="radio" id="r-3-radio" value="3" name="r-value">
                <label for="r-3-radio">3</label>
            </div>
        </fieldset>

        <button id="send-btn" type="button">Проверить</button>
        <script><%@include file="/js/form_handler.js"%></script>
    </div>
</div>

