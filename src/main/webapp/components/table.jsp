<%@ page import="com.example.weblabwork2.beans.ProcessedDot" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="dotsData" class="com.example.weblabwork2.beans.DotsData" scope="session"/>
<div class="content-block bordered">
    <% if (dotsData.isShowError()) { %>
        <div class="error-text">Данные не прошли валидацию на сервере!</div>
    <% } %>
    <table id="result-table" border="2">
        <tbody>
        <tr>
            <td><b>Время старта</b></td>
            <td><b>Время выполнения (мc)</b></td>
            <td><b>Координата X</b></td>
            <td><b>Координата Y</b></td>
            <td><b>Значение R</b></td>
            <td><b>Попадание</b></td>
        </tr>

        <%--      JSP CODE WILL BE HERE      --%>
        <%
            if (dotsData != null) {
                for (ProcessedDot dot : dotsData.getDots()) {
        %>
        <tr>
            <td> <%=dot.getProcessedAt().toLocalDateTime()%> </td>
            <td> <%=dot.getWorkTime()%> </td>
            <td class="x-cell"> <%=dot.getX()%> </td>
            <td class="y-cell"> <%=dot.getY()%> </td>
            <td class="r-cell"> <%=dot.getR()%> </td>
            <td class="colored-text hit-cell"> <b> <%=(dot.isHit() ? "+" : "-")%> </b> </td>
        </tr>
        <%
                }
            }
        %>

        </tbody>
    </table>
</div>